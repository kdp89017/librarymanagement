package com.gfg.demo.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConnectionManager {

  public static Connection getConnection() {
    try {
      return DriverManager.getConnection(
          "jdbc:mysql://localhost:3306/library", "root", "");

    } catch (Exception e) {
      System.out.println(e);
    }
    return null;
  }

}
