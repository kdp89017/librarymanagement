package com.gfg.demo.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@Entity(name = "librarian")
@Getter
@Setter
@Builder
public class Librarian {

//  @Id
//  @GeneratedValue(strategy = GenerationType.AUTO)
//  @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "librarianId")
//  @Type(type = "org.hibernate.type.Integer")
//  private int librarianId;
  private int librarianId;
  private String name;
  private String contact;
  private String password;
}
