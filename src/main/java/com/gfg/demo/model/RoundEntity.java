package com.gfg.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Type;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity(name = "scheduled_round")
@Getter
@Setter
@RequiredArgsConstructor
public class RoundEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "round_id")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID roundId;

    @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID initiatedProcessId;

    @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID panelId;

    @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID positionRoundId;

    private Integer roundNumber;
    private String meetingLink;
    private String ideLink;
    private String feedback;
    private long startTime;
    private long endTime;
    private String roundType;
    private String roundName;
    private int rating;
    private UUID feedbackId;
    private String notes;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date finishedAt;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID initiatedBy;

    @Transient private boolean isActive;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        RoundEntity that = (RoundEntity) o;
        return roundId != null && Objects.equals(roundId, that.roundId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @PrePersist
    private void createdAt() {
        this.createdAt = new Date();
        long finishDateLong = this.createdAt.getTime() +  7 * 24 * 60 * 60 * 1000;
        this.finishedAt = new Date(finishDateLong);
    }

    @PreUpdate
    private void updatedAt() {
        this.updatedAt = new Date();
    }
}
