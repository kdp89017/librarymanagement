package com.gfg.demo.repository;

import com.gfg.demo.model.Librarian;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;

@Repository
@Transactional
public interface InitiateProcessRepository extends CrudRepository<Librarian, UUID> {

    @Query(value = "Select * from initiate_process ip where ip.email = ?1 and ip.org_id = ?2", nativeQuery = true)
    List<Librarian> findByEmailAndOrgId(String email,String orgId);

    @Query(value = "Select * from initiate_process ip where ip.email = :email", nativeQuery = true)
    List<Librarian> findInterviewsOfCandidate(@Param("email") String email);

    @Query(value = "Select * from initiate_process ip where ip.email = :email and ip.status IN :statuses", nativeQuery = true)
    List<Librarian> findInterviewsOfCandidate(@Param("email") String email, @Param("statuses") Collection<String> statuses);

    @Query(value = "Select * from initiate_process ip where ip.org_id = :orgId and ip.status IN :statuses", nativeQuery = true)
    List<Librarian> findInterviewsInOrganisation(@Param("orgId") String orgId, @Param("statuses") Collection<String> statuses);

    @Query(value = "Select * from initiate_process ip where ip.org_id = :orgId", nativeQuery = true)
    List<Librarian> findInterviewsInOrganisation(@Param("orgId") String orgId);
}
