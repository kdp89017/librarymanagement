package com.gfg.demo.datastore;

import com.gfg.demo.database.ConnectionManager;
import com.gfg.demo.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StudentDataStore {

  private static final String INSERT_STUDENT = "insert into student(name,contact,branch,password) values(?,?,?,?)";

  private static Connection con = ConnectionManager.getConnection();

  public static void addnewStudent(Student student){
    try {
      PreparedStatement ps = con.prepareStatement(INSERT_STUDENT);
      // set the preparedstatement parameters
      ps.setString(1,student.getName());
      ps.setString(2,student.getContact());
      ps.setString(3,student.getBranch());
      ps.setString(4,student.getPassword());
      // call executeUpdate to execute our sql update statement and returns number of rows affected
      int updateCount = ps.executeUpdate();
      //step4 execute query
      if(updateCount>0){
        System.out.println("student inserted successfully");
      }else{
        System.out.println("failed to insert");
      }
      //step5 close the connection object
    }catch (SQLException sqlException){
      System.out.println(sqlException.getMessage());
    }
  }



  public static void main(String[] args) {
    Student s1 = Student.builder()
        .name("Shubham")
        .branch("CS")
        .password("qawsed")
        .contact("9879879870")
        .build();
    addnewStudent(s1);
  }



}
